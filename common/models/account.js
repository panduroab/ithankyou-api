var moment = require("moment"),
    async = require("async"),
    loopback = require("loopback");

module.exports = function(Account) {

    /**
     * Operation hook Account before save
     * @param  {Object} ctx
     * @param  {function} next
     * @return {undefined}
     */
    Account.observe("before save", function(ctx, next) {
        if (ctx.instance !== undefined) {
            ctx.instance.createdAt = moment().utc().format();
        } else {
            ctx.data.updatedAt = moment().utc().format();
        }
        next();
    });

    /**
     * Operation hook Account after save
     * @param  {Object} ctx
     * @param  {function} next
     * @return {undefined}
     */
    Account.observe("after save", function(ctx, next) {
        //When a new Account is created
        if (ctx.instance && "updatedAt" in ctx.instance === false) {
            //Assign a Role to the new Account
            var type = ("type" in ctx.instance) ? ctx.instance.type : "$user",
                Role = loopback.getModel("Role"),
                RoleMapping = loopback.getModel("RoleMapping"),
                account = ctx.instance;
            async.waterfall([
                //Find or create the Role by the Account type
                function findOrCreateRole(next) {
                    Role.findOrCreate({
                        where: {
                            name: type
                        }
                    }, {
                        name: type
                    }, function(err, role) {
                        if (err) return next(err);
                        next(null, role);
                    });
                },
                //Assign the Role to the Account
                function assignRole(role, next) {
                    role.principals.create({
                        principalType: RoleMapping.USER,
                        roleId: role.id,
                        principalId: account.id
                    }, function(err, principal) {
                        if (err) return next(err);
                        next(null, principal);
                    });
                }
            ], function(err, principal) {
                if (err) return console.log("Error to assign Role:", err);
                console.log("Role assignation has been succeeded:", principal);
            });
        }
        next();
    });
};
