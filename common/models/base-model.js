var moment = require("moment");
module.exports = function(BaseModel) {

    /**
     * Operation hook BaseModel before save
     * @param  {Object} ctx
     * @param  {function} next
     * @return {undefined}
     */
    BaseModel.observe("before save", function(ctx, next) {
        if (ctx.instance !== undefined) {
            ctx.instance.createdAt = moment().utc().format();
        } else {
            ctx.data.updatedAt = moment().utc().format();
        }
        next();
    });
};
