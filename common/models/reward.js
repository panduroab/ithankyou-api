var mandrill = require("mandrill-api/mandrill"),

    stripe = require('stripe')('sk_test_FoEvfAweCGbHDpl1Q3Lpu6Jt'),

    mandrill_client = new mandrill.Mandrill('-vxPycpyJMArZIrTR7yBUw');

module.exports = function (Reward) {

	Reward.makeCharge = function (payment, next) {
		console.log("CARD", payment);

		stripe.charges.create({
			amount: 90 * 100,
			currency: 'USD',
			source: payment.id,
			description: "Reward charge"
		}, function completeAppointment(err, charge) {
			if (err) {
				return next(err);
			}

			//Send email
			
			var message = {
                'html': 'Hello',
                'subject': 'Thankyou for your payment',
                'from_email': 'apanduro@itexico.net',
                'from_name': 'Abraham Panduro',
                'to': [{
                    'email': charge.source.name,
                    'name': charge.source.name,
                    'type': 'to'
                }]
            }
            mandrill_client.messages.send({
                'message': message,
                'async': true
            }, function (result) {
                console.log("The message has been delivery");
            }, function (err) {
                console.log("Error to send the message:", err);
            });
			
			next(null, charge);

		});
	};

	Reward.remoteMethod('makeCharge', {
		accepts: [{
			arg: "payment",
			type: "Object"
		}],
		returns: {
			arg: 'charge',
			type: 'Object',
			root: true
		},
		http: {
			path: '/makeCharge'
		}
	});

};