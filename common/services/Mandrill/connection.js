var Mandrill = require("mandrill-api/mandrill"),
	mandrillClient = new Mandrill.Mandrill(process.env.MANDRILL_API_KEY);

module.exports = {
	mandrillClient: mandrillClient
};