var Message = require("./libs/Message"),
	Template = require("./libs/Template");

module.exports = {
	Message: Message,
	Template: Template
};