/**
 * Mandrill Client object
 * @type {Object}
 */
var mandrillClient = require("../connection").mandrillClient;

/**
 * Message send function
 * https://mandrillapp.com/api/docs/messages.nodejs.html#method=send
 * @param  {Object}   message Mandrill Message Object
 * @param  {Objecct}   options Mandrill Options Object
 * @param  {Function} next    Callback
 * @return {undefined}
 */
function send(message, options, next) {
	//if options is a function (callback)
	if (typeof options === "function") {
		next = options;
		//Default options
		options = {
			async: false,
			ip_pool: "",
			send_at: ""
		};
	}
	/**
	 * Send Message method
	 */
	mandrillClient.messages.send({
		"message": message,
		"async": options.async,
		"ip_pool": options.ip_pool,
		"send_at": options.send_at
	}, function(result) {
		/**
		 * Return Mandrill result
		 */
		next(null, result);
	}, function(err) {
		/**
		 * Return Mandrill error
		 */
		next(err);
	});
}

/**
 * Send Template message
 * https://mandrillapp.com/api/docs/messages.nodejs.html#method=send-template
 * @param  {Object}   template Mandrill template Object
 * @param  {Object}   message  Mandrill message Object
 * @param  {Object}   options  Mandrill options Object
 * @param  {Function} next     Callback
 * @return {undefined}
 */
function sendTemplate(template, message, options, next) {
	//if options is a function (callback)
	if (typeof options === "function") {
		next = options;
		//Default options
		options = {
			async: false,
			ip_pool: "",
			send_at: ""
		};
	}
	mandrill_client.messages.sendTemplate({
		"template_name": template.template_name,
		"template_content": template.template_content,
		"message": message,
		"async": options.async,
		"ip_pool": options.ip_pool,
		"send_at": options.send_at
	}, function(result) {
		next(null, result);
	}, function(err) {
		next(err);
	});
}

/**
 * Exports all Message Functions
 * @type {Object}
 */
module.exports = {
	send: send,
	sendTemplate: sendTemplate
};