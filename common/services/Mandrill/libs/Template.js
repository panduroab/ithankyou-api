/**
 * Mandrill Client object
 * @type {Object}
 */
var mandrillClient = require("../connection").mandrillClient;

/**
 * Create a new template on Mandrill
 * https://mandrillapp.com/api/docs/templates.nodejs.html#method=add
 * @param {Object}   template Mandrill template Object
 * {
		"name": name, //Template name
		"from_email": from_email, //from_email@example.com
		"from_name": from_name, //Example Name
		"subject": subject, //example subject
		"code": code, //<div>example code</div>
		"text": text, //Example text content
		"publish": publish, //true
		"labels": labels //["example-label"]
	}
 * @param {Function} next   Callback
 */
function add(template, next) {
	mandrillClient.templates.add({
		"name": template.name,
		"from_email": template.from_email,
		"from_name": template.from_name,
		"subject": template.subject,
		"code": template.code,
		"text": ("text" in template) ? template.text : "",
		"publish": ("publish" in template) ? template.publish : false,
		"labels": ("labels" in template) ? template.labels : []
	}, function(result) {
		next(null, result);
	}, function(err) {
		next(err);
	});
}

/**
 * Get the information for an existing template
 * https://mandrillapp.com/api/docs/templates.nodejs.html#method=info
 * @param  {Object}   template {"name":"Template name"}
 * @param  {Function} next     Callback
 * @return {undefined}
 */
function info(template, next) {
	mandrillClient.templates.info(template, function(result) {
		next(null, result);
	}, function(err) {
		next(err);
	});
}

/**
 * Update a Mandrill template
 * https://mandrillapp.com/api/docs/templates.nodejs.html#method=update
 * @param  {Object}   template Mandrill tempate Object
 * {	 
		"name": name, //*Required to identify the template
		"from_email": from_email,
		"from_name": from_name,
		"subject": subject,
		"code": code,
		"text": text,
		"publish": publish,
		"labels": labels
	}
 * @param  {Function} next     Callback
 * @return {undefined}
 */
function update(template, next) {
	mandrillClient.templates.update(template, function(result) {
		next(null, result);
	}, function(err) {
		next(err);
	});
}

/**
 * Remove a Mandrill template
 * https://mandrillapp.com/api/docs/templates.nodejs.html#method=delete
 * @param  {[type]}   template Mandrill template Object
 * {name: "template name"} *required
 * @param  {Function} next     Callback
 * @return {undefined}
 */
function remove(template, next) {
	mandrillClient.templates.delete(template, function(result) {
		next(null, result);
	}, function(err) {
		next(err);
	});
}

/**
 * Return a list of all the templates available to this user
 * https://mandrillapp.com/api/docs/templates.nodejs.html#method=list
 * @param  {Object}   label An optional label to filter the templates
 * {label: "example-label"}
 * @param  {Function} next  callback
 * @return {undefined}
 */
function list(label, next) {
	//if label is a function (callback)
	if (typeof label === "function") {
		next = label;
		label = null;
	}
	mandrillClient.templates.list(label, function(result) {
		next(null, result);
	}, function(err) {
		next(err);
	});
}

/**
 * Exports all Template functions
 * @type {Object}
 */
module.exports = {
	add: add,
	info: info,
	update: update,
	remove: remove,
	list: list
};