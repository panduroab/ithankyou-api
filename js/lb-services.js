(function(window, angular, undefined) {'use strict';

var urlBase = "/api";
var authHeader = 'authorization';

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.Account
 * @header lbServices.Account
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Account` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Account",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/accounts/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__findById__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Find a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "prototype$__findById__accessTokens": {
          url: urlBase + "/accounts/:id/accessTokens/:fk",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__destroyById__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Delete a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__destroyById__accessTokens": {
          url: urlBase + "/accounts/:id/accessTokens/:fk",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__updateById__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Update a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "prototype$__updateById__accessTokens": {
          url: urlBase + "/accounts/:id/accessTokens/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Account.rewards.findById() instead.
        "prototype$__findById__rewards": {
          url: urlBase + "/accounts/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use Account.rewards.destroyById() instead.
        "prototype$__destroyById__rewards": {
          url: urlBase + "/accounts/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Account.rewards.updateById() instead.
        "prototype$__updateById__rewards": {
          url: urlBase + "/accounts/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Account.rewards.link() instead.
        "prototype$__link__rewards": {
          url: urlBase + "/accounts/:id/rewards/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Account.rewards.unlink() instead.
        "prototype$__unlink__rewards": {
          url: urlBase + "/accounts/:id/rewards/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Account.rewards.exists() instead.
        "prototype$__exists__rewards": {
          url: urlBase + "/accounts/:id/rewards/rel/:fk",
          method: "HEAD"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__get__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Queries accessTokens of Account.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "prototype$__get__accessTokens": {
          isArray: true,
          url: urlBase + "/accounts/:id/accessTokens",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__create__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "prototype$__create__accessTokens": {
          url: urlBase + "/accounts/:id/accessTokens",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__delete__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__delete__accessTokens": {
          url: urlBase + "/accounts/:id/accessTokens",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$__count__accessTokens
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Counts accessTokens of Account.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "prototype$__count__accessTokens": {
          url: urlBase + "/accounts/:id/accessTokens/count",
          method: "GET"
        },

        // INTERNAL. Use Account.rewards() instead.
        "prototype$__get__rewards": {
          isArray: true,
          url: urlBase + "/accounts/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use Account.rewards.create() instead.
        "prototype$__create__rewards": {
          url: urlBase + "/accounts/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use Account.rewards.destroyAll() instead.
        "prototype$__delete__rewards": {
          url: urlBase + "/accounts/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use Account.rewards.count() instead.
        "prototype$__count__rewards": {
          url: urlBase + "/accounts/:id/rewards/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#create
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/accounts",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#upsert
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/accounts",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#exists
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/accounts/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#findById
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/accounts/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#find
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/accounts",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#findOne
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/accounts/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#updateAll
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/accounts/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#deleteById
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/accounts/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#count
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/accounts/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#prototype$updateAttributes
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/accounts/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#login
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Login a user with username/email and password.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
         *   Default value: `user`.
         *
         *  - `rememberMe` - `boolean` - Whether the authentication credentials
         *     should be remembered in localStorage across app/browser restarts.
         *     Default: `true`.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * The response body contains properties of the AccessToken created on login.
         * Depending on the value of `include` parameter, the body may contain additional properties:
         * 
         *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
         * 
         *
         */
        "login": {
          params: {
            include: "user"
          },
          interceptor: {
            response: function(response) {
              var accessToken = response.data;
              LoopBackAuth.setUser(accessToken.id, accessToken.userId, accessToken.user);
              LoopBackAuth.rememberMe = response.config.params.rememberMe !== false;
              LoopBackAuth.save();
              return response.resource;
            }
          },
          url: urlBase + "/accounts/login",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#logout
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Logout a user with access token
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "logout": {
          interceptor: {
            response: function(response) {
              LoopBackAuth.clearUser();
              LoopBackAuth.clearStorage();
              return response.resource;
            }
          },
          url: urlBase + "/accounts/logout",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#confirm
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Confirm a user registration with email verification token
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `uid` – `{string}` - 
         *
         *  - `token` – `{string}` - 
         *
         *  - `redirect` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "confirm": {
          url: urlBase + "/accounts/confirm",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#resetPassword
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Reset password for a user with email
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "resetPassword": {
          url: urlBase + "/accounts/reset",
          method: "POST"
        },

        // INTERNAL. Use Reward.accounts.findById() instead.
        "::findById::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/:fk",
          method: "GET"
        },

        // INTERNAL. Use Reward.accounts.destroyById() instead.
        "::destroyById::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.accounts.updateById() instead.
        "::updateById::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.accounts.link() instead.
        "::link::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.accounts.unlink() instead.
        "::unlink::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.accounts.exists() instead.
        "::exists::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Reward.accounts() instead.
        "::get::Reward::accounts": {
          isArray: true,
          url: urlBase + "/rewards/:id/accounts",
          method: "GET"
        },

        // INTERNAL. Use Reward.accounts.create() instead.
        "::create::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts",
          method: "POST"
        },

        // INTERNAL. Use Reward.accounts.destroyAll() instead.
        "::delete::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.accounts.count() instead.
        "::count::Reward::accounts": {
          url: urlBase + "/rewards/:id/accounts/count",
          method: "GET"
        },

        // INTERNAL. Use AccountReward.account() instead.
        "::get::AccountReward::account": {
          url: urlBase + "/accountRewards/:id/account",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Account#getCurrent
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Get data of the currently logged user. Fail with HTTP result 401
         * when there is no user logged in.
         *
         * @param {function(Object,Object)=} successCb
         *    Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *    `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         */
        "getCurrent": {
           url: urlBase + "/accounts" + "/:id",
           method: "GET",
           params: {
             id: function() {
              var id = LoopBackAuth.currentUserId;
              if (id == null) id = '__anonymous__';
              return id;
            },
          },
          interceptor: {
            response: function(response) {
              LoopBackAuth.currentUserData = response.data;
              return response.resource;
            }
          },
          __isGetCurrentUser__ : true
        }
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Account#updateOrCreate
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Account#update
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Account#destroyById
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Account#removeById
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Account#getCachedCurrent
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link lbServices.Account#login} or
         * {@link lbServices.Account#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A Account instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name lbServices.Account#isAuthenticated
         * @methodOf lbServices.Account
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name lbServices.Account#getCurrentId
         * @methodOf lbServices.Account
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

    /**
    * @ngdoc property
    * @name lbServices.Account#modelName
    * @propertyOf lbServices.Account
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Account`.
    */
    R.modelName = "Account";

    /**
     * @ngdoc object
     * @name lbServices.Account.rewards
     * @header lbServices.Account.rewards
     * @object
     * @description
     *
     * The object `Account.rewards` groups methods
     * manipulating `Reward` instances related to `Account`.
     *
     * Call {@link lbServices.Account#rewards Account.rewards()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Account#rewards
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Queries rewards of Account.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::get::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#count
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Counts rewards of Account.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.rewards.count = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::count::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#create
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Creates a new instance in rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.create = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::create::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#destroyAll
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Deletes all rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyAll = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::delete::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#destroyById
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Delete a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::destroyById::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#exists
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Check the existence of rewards relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.exists = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::exists::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#findById
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Find a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.findById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::findById::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#link
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Add a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.link = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::link::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#unlink
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Remove the rewards relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.unlink = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::unlink::Account::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Account.rewards#updateById
         * @methodOf lbServices.Account.rewards
         *
         * @description
         *
         * Update a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.updateById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::updateById::Account::rewards"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Reward
 * @header lbServices.Reward
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Reward` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Reward",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/rewards/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Reward.template() instead.
        "prototype$__get__template": {
          url: urlBase + "/rewards/:id/template",
          method: "GET"
        },

        // INTERNAL. Use Reward.coreValues.findById() instead.
        "prototype$__findById__coreValues": {
          url: urlBase + "/rewards/:id/coreValues/:fk",
          method: "GET"
        },

        // INTERNAL. Use Reward.coreValues.destroyById() instead.
        "prototype$__destroyById__coreValues": {
          url: urlBase + "/rewards/:id/coreValues/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.coreValues.updateById() instead.
        "prototype$__updateById__coreValues": {
          url: urlBase + "/rewards/:id/coreValues/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.competences.findById() instead.
        "prototype$__findById__competences": {
          url: urlBase + "/rewards/:id/competences/:fk",
          method: "GET"
        },

        // INTERNAL. Use Reward.competences.destroyById() instead.
        "prototype$__destroyById__competences": {
          url: urlBase + "/rewards/:id/competences/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.competences.updateById() instead.
        "prototype$__updateById__competences": {
          url: urlBase + "/rewards/:id/competences/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.accounts.findById() instead.
        "prototype$__findById__accounts": {
          url: urlBase + "/rewards/:id/accounts/:fk",
          method: "GET"
        },

        // INTERNAL. Use Reward.accounts.destroyById() instead.
        "prototype$__destroyById__accounts": {
          url: urlBase + "/rewards/:id/accounts/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.accounts.updateById() instead.
        "prototype$__updateById__accounts": {
          url: urlBase + "/rewards/:id/accounts/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.accounts.link() instead.
        "prototype$__link__accounts": {
          url: urlBase + "/rewards/:id/accounts/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.accounts.unlink() instead.
        "prototype$__unlink__accounts": {
          url: urlBase + "/rewards/:id/accounts/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.accounts.exists() instead.
        "prototype$__exists__accounts": {
          url: urlBase + "/rewards/:id/accounts/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Reward.coreValues() instead.
        "prototype$__get__coreValues": {
          isArray: true,
          url: urlBase + "/rewards/:id/coreValues",
          method: "GET"
        },

        // INTERNAL. Use Reward.coreValues.create() instead.
        "prototype$__create__coreValues": {
          url: urlBase + "/rewards/:id/coreValues",
          method: "POST"
        },

        // INTERNAL. Use Reward.coreValues.destroyAll() instead.
        "prototype$__delete__coreValues": {
          url: urlBase + "/rewards/:id/coreValues",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.coreValues.count() instead.
        "prototype$__count__coreValues": {
          url: urlBase + "/rewards/:id/coreValues/count",
          method: "GET"
        },

        // INTERNAL. Use Reward.competences() instead.
        "prototype$__get__competences": {
          isArray: true,
          url: urlBase + "/rewards/:id/competences",
          method: "GET"
        },

        // INTERNAL. Use Reward.competences.create() instead.
        "prototype$__create__competences": {
          url: urlBase + "/rewards/:id/competences",
          method: "POST"
        },

        // INTERNAL. Use Reward.competences.destroyAll() instead.
        "prototype$__delete__competences": {
          url: urlBase + "/rewards/:id/competences",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.competences.count() instead.
        "prototype$__count__competences": {
          url: urlBase + "/rewards/:id/competences/count",
          method: "GET"
        },

        // INTERNAL. Use Reward.accounts() instead.
        "prototype$__get__accounts": {
          isArray: true,
          url: urlBase + "/rewards/:id/accounts",
          method: "GET"
        },

        // INTERNAL. Use Reward.accounts.create() instead.
        "prototype$__create__accounts": {
          url: urlBase + "/rewards/:id/accounts",
          method: "POST"
        },

        // INTERNAL. Use Reward.accounts.destroyAll() instead.
        "prototype$__delete__accounts": {
          url: urlBase + "/rewards/:id/accounts",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.accounts.count() instead.
        "prototype$__count__accounts": {
          url: urlBase + "/rewards/:id/accounts/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#create
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/rewards",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#upsert
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/rewards",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#exists
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/rewards/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#findById
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/rewards/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#find
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/rewards",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#findOne
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/rewards/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#updateAll
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/rewards/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#deleteById
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/rewards/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#count
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/rewards/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Reward#prototype$updateAttributes
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/rewards/:id",
          method: "PUT"
        },

        // INTERNAL. Use Account.rewards.findById() instead.
        "::findById::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use Account.rewards.destroyById() instead.
        "::destroyById::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Account.rewards.updateById() instead.
        "::updateById::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Account.rewards.link() instead.
        "::link::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Account.rewards.unlink() instead.
        "::unlink::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Account.rewards.exists() instead.
        "::exists::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Account.rewards() instead.
        "::get::Account::rewards": {
          isArray: true,
          url: urlBase + "/accounts/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use Account.rewards.create() instead.
        "::create::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use Account.rewards.destroyAll() instead.
        "::delete::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use Account.rewards.count() instead.
        "::count::Account::rewards": {
          url: urlBase + "/accounts/:id/rewards/count",
          method: "GET"
        },

        // INTERNAL. Use Template.rewards.findById() instead.
        "::findById::Template::rewards": {
          url: urlBase + "/templates/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use Template.rewards.destroyById() instead.
        "::destroyById::Template::rewards": {
          url: urlBase + "/templates/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Template.rewards.updateById() instead.
        "::updateById::Template::rewards": {
          url: urlBase + "/templates/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Template.rewards() instead.
        "::get::Template::rewards": {
          isArray: true,
          url: urlBase + "/templates/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use Template.rewards.create() instead.
        "::create::Template::rewards": {
          url: urlBase + "/templates/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use Template.rewards.destroyAll() instead.
        "::delete::Template::rewards": {
          url: urlBase + "/templates/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use Template.rewards.count() instead.
        "::count::Template::rewards": {
          url: urlBase + "/templates/:id/rewards/count",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.rewards.findById() instead.
        "::findById::CoreValue::rewards": {
          url: urlBase + "/coreValues/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.rewards.destroyById() instead.
        "::destroyById::CoreValue::rewards": {
          url: urlBase + "/coreValues/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.rewards.updateById() instead.
        "::updateById::CoreValue::rewards": {
          url: urlBase + "/coreValues/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CoreValue.rewards() instead.
        "::get::CoreValue::rewards": {
          isArray: true,
          url: urlBase + "/coreValues/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.rewards.create() instead.
        "::create::CoreValue::rewards": {
          url: urlBase + "/coreValues/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use CoreValue.rewards.destroyAll() instead.
        "::delete::CoreValue::rewards": {
          url: urlBase + "/coreValues/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.rewards.count() instead.
        "::count::CoreValue::rewards": {
          url: urlBase + "/coreValues/:id/rewards/count",
          method: "GET"
        },

        // INTERNAL. Use Competence.rewards.findById() instead.
        "::findById::Competence::rewards": {
          url: urlBase + "/competences/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use Competence.rewards.destroyById() instead.
        "::destroyById::Competence::rewards": {
          url: urlBase + "/competences/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.rewards.updateById() instead.
        "::updateById::Competence::rewards": {
          url: urlBase + "/competences/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Competence.rewards() instead.
        "::get::Competence::rewards": {
          isArray: true,
          url: urlBase + "/competences/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use Competence.rewards.create() instead.
        "::create::Competence::rewards": {
          url: urlBase + "/competences/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use Competence.rewards.destroyAll() instead.
        "::delete::Competence::rewards": {
          url: urlBase + "/competences/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.rewards.count() instead.
        "::count::Competence::rewards": {
          url: urlBase + "/competences/:id/rewards/count",
          method: "GET"
        },

        // INTERNAL. Use AccountReward.reward() instead.
        "::get::AccountReward::reward": {
          url: urlBase + "/accountRewards/:id/reward",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Reward#updateOrCreate
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Reward#update
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Reward#destroyById
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Reward#removeById
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Reward#modelName
    * @propertyOf lbServices.Reward
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Reward`.
    */
    R.modelName = "Reward";


        /**
         * @ngdoc method
         * @name lbServices.Reward#template
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Fetches belongsTo relation template.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        R.template = function() {
          var TargetResource = $injector.get("Template");
          var action = TargetResource["::get::Reward::template"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Reward.coreValues
     * @header lbServices.Reward.coreValues
     * @object
     * @description
     *
     * The object `Reward.coreValues` groups methods
     * manipulating `CoreValue` instances related to `Reward`.
     *
     * Call {@link lbServices.Reward#coreValues Reward.coreValues()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Reward#coreValues
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Queries coreValues of Reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::get::Reward::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.coreValues#count
         * @methodOf lbServices.Reward.coreValues
         *
         * @description
         *
         * Counts coreValues of Reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.coreValues.count = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::count::Reward::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.coreValues#create
         * @methodOf lbServices.Reward.coreValues
         *
         * @description
         *
         * Creates a new instance in coreValues of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues.create = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::create::Reward::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.coreValues#destroyAll
         * @methodOf lbServices.Reward.coreValues
         *
         * @description
         *
         * Deletes all coreValues of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.coreValues.destroyAll = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::delete::Reward::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.coreValues#destroyById
         * @methodOf lbServices.Reward.coreValues
         *
         * @description
         *
         * Delete a related item by id for coreValues.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for coreValues
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.coreValues.destroyById = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::destroyById::Reward::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.coreValues#findById
         * @methodOf lbServices.Reward.coreValues
         *
         * @description
         *
         * Find a related item by id for coreValues.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for coreValues
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues.findById = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::findById::Reward::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.coreValues#updateById
         * @methodOf lbServices.Reward.coreValues
         *
         * @description
         *
         * Update a related item by id for coreValues.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for coreValues
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues.updateById = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::updateById::Reward::coreValues"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Reward.competences
     * @header lbServices.Reward.competences
     * @object
     * @description
     *
     * The object `Reward.competences` groups methods
     * manipulating `Competence` instances related to `Reward`.
     *
     * Call {@link lbServices.Reward#competences Reward.competences()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Reward#competences
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Queries competences of Reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::get::Reward::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.competences#count
         * @methodOf lbServices.Reward.competences
         *
         * @description
         *
         * Counts competences of Reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.competences.count = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::count::Reward::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.competences#create
         * @methodOf lbServices.Reward.competences
         *
         * @description
         *
         * Creates a new instance in competences of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences.create = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::create::Reward::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.competences#destroyAll
         * @methodOf lbServices.Reward.competences
         *
         * @description
         *
         * Deletes all competences of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.competences.destroyAll = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::delete::Reward::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.competences#destroyById
         * @methodOf lbServices.Reward.competences
         *
         * @description
         *
         * Delete a related item by id for competences.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for competences
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.competences.destroyById = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::destroyById::Reward::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.competences#findById
         * @methodOf lbServices.Reward.competences
         *
         * @description
         *
         * Find a related item by id for competences.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for competences
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences.findById = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::findById::Reward::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.competences#updateById
         * @methodOf lbServices.Reward.competences
         *
         * @description
         *
         * Update a related item by id for competences.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for competences
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences.updateById = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::updateById::Reward::competences"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Reward.accounts
     * @header lbServices.Reward.accounts
     * @object
     * @description
     *
     * The object `Reward.accounts` groups methods
     * manipulating `Account` instances related to `Reward`.
     *
     * Call {@link lbServices.Reward#accounts Reward.accounts()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Reward#accounts
         * @methodOf lbServices.Reward
         *
         * @description
         *
         * Queries accounts of Reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.accounts = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::get::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#count
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Counts accounts of Reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.accounts.count = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::count::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#create
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Creates a new instance in accounts of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.accounts.create = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::create::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#destroyAll
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Deletes all accounts of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.accounts.destroyAll = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::delete::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#destroyById
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Delete a related item by id for accounts.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for accounts
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.accounts.destroyById = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::destroyById::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#exists
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Check the existence of accounts relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for accounts
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.accounts.exists = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::exists::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#findById
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Find a related item by id for accounts.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for accounts
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.accounts.findById = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::findById::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#link
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Add a related item by id for accounts.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for accounts
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.accounts.link = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::link::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#unlink
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Remove the accounts relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for accounts
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.accounts.unlink = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::unlink::Reward::accounts"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Reward.accounts#updateById
         * @methodOf lbServices.Reward.accounts
         *
         * @description
         *
         * Update a related item by id for accounts.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for accounts
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.accounts.updateById = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::updateById::Reward::accounts"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Template
 * @header lbServices.Template
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Template` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Template",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/templates/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Template.rewards.findById() instead.
        "prototype$__findById__rewards": {
          url: urlBase + "/templates/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use Template.rewards.destroyById() instead.
        "prototype$__destroyById__rewards": {
          url: urlBase + "/templates/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Template.rewards.updateById() instead.
        "prototype$__updateById__rewards": {
          url: urlBase + "/templates/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Template.rewards() instead.
        "prototype$__get__rewards": {
          isArray: true,
          url: urlBase + "/templates/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use Template.rewards.create() instead.
        "prototype$__create__rewards": {
          url: urlBase + "/templates/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use Template.rewards.destroyAll() instead.
        "prototype$__delete__rewards": {
          url: urlBase + "/templates/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use Template.rewards.count() instead.
        "prototype$__count__rewards": {
          url: urlBase + "/templates/:id/rewards/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#create
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/templates",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#upsert
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/templates",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#exists
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/templates/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#findById
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/templates/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#find
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/templates",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#findOne
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/templates/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#updateAll
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/templates/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#deleteById
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/templates/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#count
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/templates/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Template#prototype$updateAttributes
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/templates/:id",
          method: "PUT"
        },

        // INTERNAL. Use Reward.template() instead.
        "::get::Reward::template": {
          url: urlBase + "/rewards/:id/template",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Template#updateOrCreate
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Template` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Template#update
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Template#destroyById
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Template#removeById
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Template#modelName
    * @propertyOf lbServices.Template
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Template`.
    */
    R.modelName = "Template";

    /**
     * @ngdoc object
     * @name lbServices.Template.rewards
     * @header lbServices.Template.rewards
     * @object
     * @description
     *
     * The object `Template.rewards` groups methods
     * manipulating `Reward` instances related to `Template`.
     *
     * Call {@link lbServices.Template#rewards Template.rewards()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Template#rewards
         * @methodOf lbServices.Template
         *
         * @description
         *
         * Queries rewards of Template.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::get::Template::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Template.rewards#count
         * @methodOf lbServices.Template.rewards
         *
         * @description
         *
         * Counts rewards of Template.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.rewards.count = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::count::Template::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Template.rewards#create
         * @methodOf lbServices.Template.rewards
         *
         * @description
         *
         * Creates a new instance in rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.create = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::create::Template::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Template.rewards#destroyAll
         * @methodOf lbServices.Template.rewards
         *
         * @description
         *
         * Deletes all rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyAll = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::delete::Template::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Template.rewards#destroyById
         * @methodOf lbServices.Template.rewards
         *
         * @description
         *
         * Delete a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::destroyById::Template::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Template.rewards#findById
         * @methodOf lbServices.Template.rewards
         *
         * @description
         *
         * Find a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.findById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::findById::Template::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Template.rewards#updateById
         * @methodOf lbServices.Template.rewards
         *
         * @description
         *
         * Update a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.updateById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::updateById::Template::rewards"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.CoreValue
 * @header lbServices.CoreValue
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `CoreValue` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "CoreValue",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/coreValues/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use CoreValue.rewards.findById() instead.
        "prototype$__findById__rewards": {
          url: urlBase + "/coreValues/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.rewards.destroyById() instead.
        "prototype$__destroyById__rewards": {
          url: urlBase + "/coreValues/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.rewards.updateById() instead.
        "prototype$__updateById__rewards": {
          url: urlBase + "/coreValues/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CoreValue.competences.findById() instead.
        "prototype$__findById__competences": {
          url: urlBase + "/coreValues/:id/competences/:fk",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.competences.destroyById() instead.
        "prototype$__destroyById__competences": {
          url: urlBase + "/coreValues/:id/competences/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.competences.updateById() instead.
        "prototype$__updateById__competences": {
          url: urlBase + "/coreValues/:id/competences/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CoreValue.rewards() instead.
        "prototype$__get__rewards": {
          isArray: true,
          url: urlBase + "/coreValues/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.rewards.create() instead.
        "prototype$__create__rewards": {
          url: urlBase + "/coreValues/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use CoreValue.rewards.destroyAll() instead.
        "prototype$__delete__rewards": {
          url: urlBase + "/coreValues/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.rewards.count() instead.
        "prototype$__count__rewards": {
          url: urlBase + "/coreValues/:id/rewards/count",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.competences() instead.
        "prototype$__get__competences": {
          isArray: true,
          url: urlBase + "/coreValues/:id/competences",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.competences.create() instead.
        "prototype$__create__competences": {
          url: urlBase + "/coreValues/:id/competences",
          method: "POST"
        },

        // INTERNAL. Use CoreValue.competences.destroyAll() instead.
        "prototype$__delete__competences": {
          url: urlBase + "/coreValues/:id/competences",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.competences.count() instead.
        "prototype$__count__competences": {
          url: urlBase + "/coreValues/:id/competences/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#create
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/coreValues",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#upsert
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/coreValues",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#exists
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/coreValues/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#findById
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/coreValues/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#find
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/coreValues",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#findOne
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/coreValues/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#updateAll
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/coreValues/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#deleteById
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/coreValues/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#count
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/coreValues/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#prototype$updateAttributes
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/coreValues/:id",
          method: "PUT"
        },

        // INTERNAL. Use Reward.coreValues.findById() instead.
        "::findById::Reward::coreValues": {
          url: urlBase + "/rewards/:id/coreValues/:fk",
          method: "GET"
        },

        // INTERNAL. Use Reward.coreValues.destroyById() instead.
        "::destroyById::Reward::coreValues": {
          url: urlBase + "/rewards/:id/coreValues/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.coreValues.updateById() instead.
        "::updateById::Reward::coreValues": {
          url: urlBase + "/rewards/:id/coreValues/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.coreValues() instead.
        "::get::Reward::coreValues": {
          isArray: true,
          url: urlBase + "/rewards/:id/coreValues",
          method: "GET"
        },

        // INTERNAL. Use Reward.coreValues.create() instead.
        "::create::Reward::coreValues": {
          url: urlBase + "/rewards/:id/coreValues",
          method: "POST"
        },

        // INTERNAL. Use Reward.coreValues.destroyAll() instead.
        "::delete::Reward::coreValues": {
          url: urlBase + "/rewards/:id/coreValues",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.coreValues.count() instead.
        "::count::Reward::coreValues": {
          url: urlBase + "/rewards/:id/coreValues/count",
          method: "GET"
        },

        // INTERNAL. Use Competence.coreValues.findById() instead.
        "::findById::Competence::coreValues": {
          url: urlBase + "/competences/:id/coreValues/:fk",
          method: "GET"
        },

        // INTERNAL. Use Competence.coreValues.destroyById() instead.
        "::destroyById::Competence::coreValues": {
          url: urlBase + "/competences/:id/coreValues/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.coreValues.updateById() instead.
        "::updateById::Competence::coreValues": {
          url: urlBase + "/competences/:id/coreValues/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Competence.coreValues() instead.
        "::get::Competence::coreValues": {
          isArray: true,
          url: urlBase + "/competences/:id/coreValues",
          method: "GET"
        },

        // INTERNAL. Use Competence.coreValues.create() instead.
        "::create::Competence::coreValues": {
          url: urlBase + "/competences/:id/coreValues",
          method: "POST"
        },

        // INTERNAL. Use Competence.coreValues.destroyAll() instead.
        "::delete::Competence::coreValues": {
          url: urlBase + "/competences/:id/coreValues",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.coreValues.count() instead.
        "::count::Competence::coreValues": {
          url: urlBase + "/competences/:id/coreValues/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.CoreValue#updateOrCreate
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#update
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#destroyById
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.CoreValue#removeById
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.CoreValue#modelName
    * @propertyOf lbServices.CoreValue
    * @description
    * The name of the model represented by this $resource,
    * i.e. `CoreValue`.
    */
    R.modelName = "CoreValue";

    /**
     * @ngdoc object
     * @name lbServices.CoreValue.rewards
     * @header lbServices.CoreValue.rewards
     * @object
     * @description
     *
     * The object `CoreValue.rewards` groups methods
     * manipulating `Reward` instances related to `CoreValue`.
     *
     * Call {@link lbServices.CoreValue#rewards CoreValue.rewards()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CoreValue#rewards
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Queries rewards of CoreValue.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::get::CoreValue::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.rewards#count
         * @methodOf lbServices.CoreValue.rewards
         *
         * @description
         *
         * Counts rewards of CoreValue.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.rewards.count = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::count::CoreValue::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.rewards#create
         * @methodOf lbServices.CoreValue.rewards
         *
         * @description
         *
         * Creates a new instance in rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.create = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::create::CoreValue::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.rewards#destroyAll
         * @methodOf lbServices.CoreValue.rewards
         *
         * @description
         *
         * Deletes all rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyAll = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::delete::CoreValue::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.rewards#destroyById
         * @methodOf lbServices.CoreValue.rewards
         *
         * @description
         *
         * Delete a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::destroyById::CoreValue::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.rewards#findById
         * @methodOf lbServices.CoreValue.rewards
         *
         * @description
         *
         * Find a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.findById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::findById::CoreValue::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.rewards#updateById
         * @methodOf lbServices.CoreValue.rewards
         *
         * @description
         *
         * Update a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.updateById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::updateById::CoreValue::rewards"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.CoreValue.competences
     * @header lbServices.CoreValue.competences
     * @object
     * @description
     *
     * The object `CoreValue.competences` groups methods
     * manipulating `Competence` instances related to `CoreValue`.
     *
     * Call {@link lbServices.CoreValue#competences CoreValue.competences()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CoreValue#competences
         * @methodOf lbServices.CoreValue
         *
         * @description
         *
         * Queries competences of CoreValue.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::get::CoreValue::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.competences#count
         * @methodOf lbServices.CoreValue.competences
         *
         * @description
         *
         * Counts competences of CoreValue.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.competences.count = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::count::CoreValue::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.competences#create
         * @methodOf lbServices.CoreValue.competences
         *
         * @description
         *
         * Creates a new instance in competences of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences.create = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::create::CoreValue::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.competences#destroyAll
         * @methodOf lbServices.CoreValue.competences
         *
         * @description
         *
         * Deletes all competences of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.competences.destroyAll = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::delete::CoreValue::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.competences#destroyById
         * @methodOf lbServices.CoreValue.competences
         *
         * @description
         *
         * Delete a related item by id for competences.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for competences
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.competences.destroyById = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::destroyById::CoreValue::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.competences#findById
         * @methodOf lbServices.CoreValue.competences
         *
         * @description
         *
         * Find a related item by id for competences.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for competences
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences.findById = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::findById::CoreValue::competences"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CoreValue.competences#updateById
         * @methodOf lbServices.CoreValue.competences
         *
         * @description
         *
         * Update a related item by id for competences.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for competences
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R.competences.updateById = function() {
          var TargetResource = $injector.get("Competence");
          var action = TargetResource["::updateById::CoreValue::competences"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Competence
 * @header lbServices.Competence
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Competence` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Competence",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/competences/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Competence.coreValues.findById() instead.
        "prototype$__findById__coreValues": {
          url: urlBase + "/competences/:id/coreValues/:fk",
          method: "GET"
        },

        // INTERNAL. Use Competence.coreValues.destroyById() instead.
        "prototype$__destroyById__coreValues": {
          url: urlBase + "/competences/:id/coreValues/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.coreValues.updateById() instead.
        "prototype$__updateById__coreValues": {
          url: urlBase + "/competences/:id/coreValues/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Competence.rewards.findById() instead.
        "prototype$__findById__rewards": {
          url: urlBase + "/competences/:id/rewards/:fk",
          method: "GET"
        },

        // INTERNAL. Use Competence.rewards.destroyById() instead.
        "prototype$__destroyById__rewards": {
          url: urlBase + "/competences/:id/rewards/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.rewards.updateById() instead.
        "prototype$__updateById__rewards": {
          url: urlBase + "/competences/:id/rewards/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Competence.coreValues() instead.
        "prototype$__get__coreValues": {
          isArray: true,
          url: urlBase + "/competences/:id/coreValues",
          method: "GET"
        },

        // INTERNAL. Use Competence.coreValues.create() instead.
        "prototype$__create__coreValues": {
          url: urlBase + "/competences/:id/coreValues",
          method: "POST"
        },

        // INTERNAL. Use Competence.coreValues.destroyAll() instead.
        "prototype$__delete__coreValues": {
          url: urlBase + "/competences/:id/coreValues",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.coreValues.count() instead.
        "prototype$__count__coreValues": {
          url: urlBase + "/competences/:id/coreValues/count",
          method: "GET"
        },

        // INTERNAL. Use Competence.rewards() instead.
        "prototype$__get__rewards": {
          isArray: true,
          url: urlBase + "/competences/:id/rewards",
          method: "GET"
        },

        // INTERNAL. Use Competence.rewards.create() instead.
        "prototype$__create__rewards": {
          url: urlBase + "/competences/:id/rewards",
          method: "POST"
        },

        // INTERNAL. Use Competence.rewards.destroyAll() instead.
        "prototype$__delete__rewards": {
          url: urlBase + "/competences/:id/rewards",
          method: "DELETE"
        },

        // INTERNAL. Use Competence.rewards.count() instead.
        "prototype$__count__rewards": {
          url: urlBase + "/competences/:id/rewards/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#create
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/competences",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#upsert
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/competences",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#exists
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/competences/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#findById
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/competences/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#find
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/competences",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#findOne
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/competences/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#updateAll
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/competences/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#deleteById
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/competences/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#count
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/competences/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Competence#prototype$updateAttributes
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/competences/:id",
          method: "PUT"
        },

        // INTERNAL. Use Reward.competences.findById() instead.
        "::findById::Reward::competences": {
          url: urlBase + "/rewards/:id/competences/:fk",
          method: "GET"
        },

        // INTERNAL. Use Reward.competences.destroyById() instead.
        "::destroyById::Reward::competences": {
          url: urlBase + "/rewards/:id/competences/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.competences.updateById() instead.
        "::updateById::Reward::competences": {
          url: urlBase + "/rewards/:id/competences/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Reward.competences() instead.
        "::get::Reward::competences": {
          isArray: true,
          url: urlBase + "/rewards/:id/competences",
          method: "GET"
        },

        // INTERNAL. Use Reward.competences.create() instead.
        "::create::Reward::competences": {
          url: urlBase + "/rewards/:id/competences",
          method: "POST"
        },

        // INTERNAL. Use Reward.competences.destroyAll() instead.
        "::delete::Reward::competences": {
          url: urlBase + "/rewards/:id/competences",
          method: "DELETE"
        },

        // INTERNAL. Use Reward.competences.count() instead.
        "::count::Reward::competences": {
          url: urlBase + "/rewards/:id/competences/count",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.competences.findById() instead.
        "::findById::CoreValue::competences": {
          url: urlBase + "/coreValues/:id/competences/:fk",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.competences.destroyById() instead.
        "::destroyById::CoreValue::competences": {
          url: urlBase + "/coreValues/:id/competences/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.competences.updateById() instead.
        "::updateById::CoreValue::competences": {
          url: urlBase + "/coreValues/:id/competences/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CoreValue.competences() instead.
        "::get::CoreValue::competences": {
          isArray: true,
          url: urlBase + "/coreValues/:id/competences",
          method: "GET"
        },

        // INTERNAL. Use CoreValue.competences.create() instead.
        "::create::CoreValue::competences": {
          url: urlBase + "/coreValues/:id/competences",
          method: "POST"
        },

        // INTERNAL. Use CoreValue.competences.destroyAll() instead.
        "::delete::CoreValue::competences": {
          url: urlBase + "/coreValues/:id/competences",
          method: "DELETE"
        },

        // INTERNAL. Use CoreValue.competences.count() instead.
        "::count::CoreValue::competences": {
          url: urlBase + "/coreValues/:id/competences/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Competence#updateOrCreate
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Competence` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Competence#update
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Competence#destroyById
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Competence#removeById
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Competence#modelName
    * @propertyOf lbServices.Competence
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Competence`.
    */
    R.modelName = "Competence";

    /**
     * @ngdoc object
     * @name lbServices.Competence.coreValues
     * @header lbServices.Competence.coreValues
     * @object
     * @description
     *
     * The object `Competence.coreValues` groups methods
     * manipulating `CoreValue` instances related to `Competence`.
     *
     * Call {@link lbServices.Competence#coreValues Competence.coreValues()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Competence#coreValues
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Queries coreValues of Competence.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::get::Competence::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.coreValues#count
         * @methodOf lbServices.Competence.coreValues
         *
         * @description
         *
         * Counts coreValues of Competence.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.coreValues.count = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::count::Competence::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.coreValues#create
         * @methodOf lbServices.Competence.coreValues
         *
         * @description
         *
         * Creates a new instance in coreValues of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues.create = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::create::Competence::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.coreValues#destroyAll
         * @methodOf lbServices.Competence.coreValues
         *
         * @description
         *
         * Deletes all coreValues of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.coreValues.destroyAll = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::delete::Competence::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.coreValues#destroyById
         * @methodOf lbServices.Competence.coreValues
         *
         * @description
         *
         * Delete a related item by id for coreValues.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for coreValues
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.coreValues.destroyById = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::destroyById::Competence::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.coreValues#findById
         * @methodOf lbServices.Competence.coreValues
         *
         * @description
         *
         * Find a related item by id for coreValues.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for coreValues
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues.findById = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::findById::Competence::coreValues"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.coreValues#updateById
         * @methodOf lbServices.Competence.coreValues
         *
         * @description
         *
         * Update a related item by id for coreValues.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for coreValues
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CoreValue` object.)
         * </em>
         */
        R.coreValues.updateById = function() {
          var TargetResource = $injector.get("CoreValue");
          var action = TargetResource["::updateById::Competence::coreValues"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Competence.rewards
     * @header lbServices.Competence.rewards
     * @object
     * @description
     *
     * The object `Competence.rewards` groups methods
     * manipulating `Reward` instances related to `Competence`.
     *
     * Call {@link lbServices.Competence#rewards Competence.rewards()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Competence#rewards
         * @methodOf lbServices.Competence
         *
         * @description
         *
         * Queries rewards of Competence.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::get::Competence::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.rewards#count
         * @methodOf lbServices.Competence.rewards
         *
         * @description
         *
         * Counts rewards of Competence.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.rewards.count = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::count::Competence::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.rewards#create
         * @methodOf lbServices.Competence.rewards
         *
         * @description
         *
         * Creates a new instance in rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.create = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::create::Competence::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.rewards#destroyAll
         * @methodOf lbServices.Competence.rewards
         *
         * @description
         *
         * Deletes all rewards of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyAll = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::delete::Competence::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.rewards#destroyById
         * @methodOf lbServices.Competence.rewards
         *
         * @description
         *
         * Delete a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.rewards.destroyById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::destroyById::Competence::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.rewards#findById
         * @methodOf lbServices.Competence.rewards
         *
         * @description
         *
         * Find a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.findById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::findById::Competence::rewards"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Competence.rewards#updateById
         * @methodOf lbServices.Competence.rewards
         *
         * @description
         *
         * Update a related item by id for rewards.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `fk` – `{*}` - Foreign key for rewards
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.rewards.updateById = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::updateById::Competence::rewards"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.AccountReward
 * @header lbServices.AccountReward
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `AccountReward` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "AccountReward",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/accountRewards/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use AccountReward.account() instead.
        "prototype$__get__account": {
          url: urlBase + "/accountRewards/:id/account",
          method: "GET"
        },

        // INTERNAL. Use AccountReward.reward() instead.
        "prototype$__get__reward": {
          url: urlBase + "/accountRewards/:id/reward",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#create
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/accountRewards",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#upsert
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/accountRewards",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#exists
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/accountRewards/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#findById
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/accountRewards/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#find
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/accountRewards",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#findOne
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/accountRewards/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#updateAll
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/accountRewards/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#deleteById
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/accountRewards/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#count
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/accountRewards/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#prototype$updateAttributes
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/accountRewards/:id",
          method: "PUT"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.AccountReward#updateOrCreate
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccountReward` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#update
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#destroyById
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#removeById
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.AccountReward#modelName
    * @propertyOf lbServices.AccountReward
    * @description
    * The name of the model represented by this $resource,
    * i.e. `AccountReward`.
    */
    R.modelName = "AccountReward";


        /**
         * @ngdoc method
         * @name lbServices.AccountReward#account
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Fetches belongsTo relation account.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Account` object.)
         * </em>
         */
        R.account = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::get::AccountReward::account"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.AccountReward#reward
         * @methodOf lbServices.AccountReward
         *
         * @description
         *
         * Fetches belongsTo relation reward.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - BaseModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Reward` object.)
         * </em>
         */
        R.reward = function() {
          var TargetResource = $injector.get("Reward");
          var action = TargetResource["::get::AccountReward::reward"];
          return action.apply(R, arguments);
        };

    return R;
  }]);


module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    }

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = propsPrefix + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {

          // filter out non urlBase requests
          if (config.url.substr(0, urlBase.length) !== urlBase) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 } },
              status: 401,
              config: config,
              headers: function() { return undefined; }
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        }
      }
    }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
    };

    this.$get = ['$resource', function($resource) {
      return function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };
    }];
  });

})(window, window.angular);
