var loopback = require('loopback'),
    boot = require('loopback-boot'),
    path = require('path');

var app = module.exports = loopback();

app.use('/bower_components/', loopback.static(path.resolve(__dirname, '../client/bower_components')));
app.use('/payment.html', loopback.static(path.resolve(__dirname, '../client/payment.html')));
app.use('/lb-services.js', loopback.static(path.resolve(__dirname, '../js/lb-services.js')));

app.start = function() {
    // start the web server
    return app.listen(function() {
        app.emit('started');
        console.log('Web server listening at: %s', app.get('url'));
    });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
    if (err) throw err;

    // start the server if `$ node server.js`
    if (require.main === module)
        app.start();
});
